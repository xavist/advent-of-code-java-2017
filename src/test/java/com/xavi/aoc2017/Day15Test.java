package com.xavi.aoc2017;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day15Test {
    @Test
    public void test1() {
        final Day15 day15 = new Day15(65, 8921);
        assertEquals(588, day15.matches1());
        assertEquals(309, day15.matches2());
    }

    @Test
    public void solution() {
        final Day15 day15 = new Day15(634, 301);
        assertEquals(573, day15.matches1());
        assertEquals(294, day15.matches2());
    }
}