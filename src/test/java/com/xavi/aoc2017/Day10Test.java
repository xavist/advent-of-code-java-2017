package com.xavi.aoc2017;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day10Test {
    @Test
    public void hash1Test1() {
        final Day10 day10 = new Day10("3,4,1,5", 5);
        assertEquals(12, day10.round());
    }

    @Test
    public void hash2Test1() {
        final Day10 day10 = new Day10("");
        assertEquals("a2582a3a0e66e6e86e3812dcb672a272", day10.knotHash());
    }

    @Test
    public void hash2Test2() {
        final Day10 day10 = new Day10("AoC 2017");
        assertEquals("33efeb34ea91902bb2f59c9920caa6cd", day10.knotHash());
    }

    @Test
    public void hash2Test3() {
        final Day10 day10 = new Day10("1,2,3");
        assertEquals("3efbe78a8d82f29979031a4aa0b16a9d", day10.knotHash());
    }

    @Test
    public void hash2Test4() {
        final Day10 day10 = new Day10("1,2,4");
        assertEquals("63960835bcdc130f0b66d7ff4f6a5a8e", day10.knotHash());
    }

    @Test
    public void solution() {
        final Day10 day10 = new Day10("187,254,0,81,169,219,1,190,19,102,255,56,46,32,2,216");
        assertEquals(1980, day10.round());
        assertEquals("899124dac21012ebc32e2f4d11eaec55", day10.knotHash());
    }
}