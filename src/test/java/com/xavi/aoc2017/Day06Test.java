package com.xavi.aoc2017;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day06Test {
    @Test
    public void test1() {
        final Day06 day06 = new Day06("0 2 7 0");
        final int[] redistribution = day06.redistribution();
        assertEquals(5, redistribution[0]);
        assertEquals(4, redistribution[1]);
    }

    @Test
    public void solution() {
        final Day06 day06 = new Day06("14 0 15 12 11 11 3 5 1 6 8 4 9 1 8 4");
        final int[] redistribution = day06.redistribution();
        assertEquals(11137, redistribution[0]);
        assertEquals(1037, redistribution[1]);
    }
}