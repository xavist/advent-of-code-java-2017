package com.xavi.aoc2017;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day24Test {

    @Test
    public void test1() {
        final Day24 day24 = new Day24("0/2\n" +
                                        "2/2\n" +
                                        "2/3\n" +
                                        "3/4\n" +
                                        "3/5\n" +
                                        "0/1\n" +
                                        "10/1\n" +
                                        "9/10");
        assertEquals(31, day24.strongest());
        assertEquals(19, day24.longest());
    }

    @Test
    public void solution() {
        final Day24 day24 = new Day24("48/5\n" +
                                        "25/10\n" +
                                        "35/49\n" +
                                        "34/41\n" +
                                        "35/35\n" +
                                        "47/35\n" +
                                        "34/46\n" +
                                        "47/23\n" +
                                        "28/8\n" +
                                        "27/21\n" +
                                        "40/11\n" +
                                        "22/50\n" +
                                        "48/42\n" +
                                        "38/17\n" +
                                        "50/33\n" +
                                        "13/13\n" +
                                        "22/33\n" +
                                        "17/29\n" +
                                        "50/0\n" +
                                        "20/47\n" +
                                        "28/0\n" +
                                        "42/4\n" +
                                        "46/22\n" +
                                        "19/35\n" +
                                        "17/22\n" +
                                        "33/37\n" +
                                        "47/7\n" +
                                        "35/20\n" +
                                        "8/36\n" +
                                        "24/34\n" +
                                        "6/7\n" +
                                        "7/43\n" +
                                        "45/37\n" +
                                        "21/31\n" +
                                        "37/26\n" +
                                        "16/5\n" +
                                        "11/14\n" +
                                        "7/23\n" +
                                        "2/23\n" +
                                        "3/25\n" +
                                        "20/20\n" +
                                        "18/20\n" +
                                        "19/34\n" +
                                        "25/46\n" +
                                        "41/24\n" +
                                        "0/33\n" +
                                        "3/7\n" +
                                        "49/38\n" +
                                        "47/22\n" +
                                        "44/15\n" +
                                        "24/21\n" +
                                        "10/35\n" +
                                        "6/21\n" +
                                        "14/50");
        assertEquals(1656, day24.strongest());
        assertEquals(1642, day24.longest());
    }
}