package com.xavi.aoc2017;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day03Test {
    @Test
    public void test1() {
        final Day03 day03 = new Day03(1);
        assertEquals(0, day03.distance());
        assertEquals(2, day03.stressTest());
    }

    @Test
    public void test2() {
        final Day03 day03 = new Day03(12);
        assertEquals(3, day03.distance());
        assertEquals(23, day03.stressTest());
    }

    @Test
    public void test3() {
        final Day03 day03 = new Day03(36);
        assertEquals(5, day03.distance());
        assertEquals(54, day03.stressTest());
    }

    @Test
    public void test4() {
        final Day03 day03 = new Day03(23);
        assertEquals(2, day03.distance());
        assertEquals(25, day03.stressTest());
    }

    @Test
    public void test5() {
        final Day03 day03 = new Day03(1024);
        assertEquals(31, day03.distance());
        assertEquals(1968, day03.stressTest());
    }

    @Test
    public void solution() {
        final Day03 day03 = new Day03(347991);
        assertEquals(480, day03.distance());
        assertEquals(349975, day03.stressTest());
    }
}