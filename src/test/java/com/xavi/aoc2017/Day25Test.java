package com.xavi.aoc2017;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day25Test {

    @Test
    public void test1() {
        final Day25 day25 = new Day25("A", 6,
                                        "In state A:\n" +
                                        "  If the current value is 0:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the right.\n" +
                                        "    - Continue with state B.\n" +
                                        "  If the current value is 1:\n" +
                                        "    - Write the value 0.\n" +
                                        "    - Move one slot to the left.\n" +
                                        "    - Continue with state B.\n" +
                                        "\n" +
                                        "In state B:\n" +
                                        "  If the current value is 0:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the left.\n" +
                                        "    - Continue with state A.\n" +
                                        "  If the current value is 1:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the right.\n" +
                                        "    - Continue with state A.");
        assertEquals(3, day25.checksum());
    }

    @Test
    public void solution() {
        final Day25 day25 = new Day25("A", 12919244,
                                        "In state A:\n" +
                                        "  If the current value is 0:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the right.\n" +
                                        "    - Continue with state B.\n" +
                                        "  If the current value is 1:\n" +
                                        "    - Write the value 0.\n" +
                                        "    - Move one slot to the left.\n" +
                                        "    - Continue with state C.\n" +
                                        "\n" +
                                        "In state B:\n" +
                                        "  If the current value is 0:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the left.\n" +
                                        "    - Continue with state A.\n" +
                                        "  If the current value is 1:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the right.\n" +
                                        "    - Continue with state D.\n" +
                                        "\n" +
                                        "In state C:\n" +
                                        "  If the current value is 0:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the right.\n" +
                                        "    - Continue with state A.\n" +
                                        "  If the current value is 1:\n" +
                                        "    - Write the value 0.\n" +
                                        "    - Move one slot to the left.\n" +
                                        "    - Continue with state E.\n" +
                                        "\n" +
                                        "In state D:\n" +
                                        "  If the current value is 0:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the right.\n" +
                                        "    - Continue with state A.\n" +
                                        "  If the current value is 1:\n" +
                                        "    - Write the value 0.\n" +
                                        "    - Move one slot to the right.\n" +
                                        "    - Continue with state B.\n" +
                                        "\n" +
                                        "In state E:\n" +
                                        "  If the current value is 0:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the left.\n" +
                                        "    - Continue with state F.\n" +
                                        "  If the current value is 1:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the left.\n" +
                                        "    - Continue with state C.\n" +
                                        "\n" +
                                        "In state F:\n" +
                                        "  If the current value is 0:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the right.\n" +
                                        "    - Continue with state D.\n" +
                                        "  If the current value is 1:\n" +
                                        "    - Write the value 1.\n" +
                                        "    - Move one slot to the right.\n" +
                                        "    - Continue with state A.");
        assertEquals(4287, day25.checksum());
    }
}