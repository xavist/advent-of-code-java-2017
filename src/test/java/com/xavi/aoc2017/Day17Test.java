package com.xavi.aoc2017;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day17Test {
    @Test
    public void test1() {
        final Day17 day17 = new Day17(3);
        assertEquals(638, day17.value1());
    }

    @Test
    public void solution() {
        final Day17 day17 = new Day17(386);
        assertEquals(419, day17.value1());
        assertEquals(46038988, day17.value2());
    }
}