package com.xavi.aoc2017;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day14Test {
    @Test
    public void test1() {
        final Day14 day14 = new Day14("flqrgnkx");
        final int[] info = day14.info();
        assertEquals(8108, info[0]);
        assertEquals(1242, info[1]);
    }

    @Test
    public void solution() {
        final Day14 day14 = new Day14("hwlqcszp");
        final int[] info = day14.info();
        assertEquals(8304, info[0]);
        assertEquals(1018, info[1]);
    }
}