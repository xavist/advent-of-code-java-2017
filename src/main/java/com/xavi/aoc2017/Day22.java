package com.xavi.aoc2017;


import java.util.HashMap;
import java.util.Map;

import static com.xavi.aoc2017.Day22.Direction.N;
import static java.util.Arrays.stream;

public class Day22 {
    private static final char CLEAN ='.';
    private static final char INFECTED ='#';
    private static final char WEAKENED ='W';
    private static final char FLAGGED ='F';

    private final String input;

    enum Direction {
        N(-1, 0),
        E(0, 1),
        S(1, 0),
        W(0, -1);

        private static final Direction[] VALUES = values();

        private final int x;
        private final int y;

        private Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int[] nextPosition(int[] currentPosition) {
            int[] nextPosition = {currentPosition[0] + x, currentPosition[1] + y};
            return nextPosition;
        }

        public Direction left() {
            return VALUES[index(ordinal() + VALUES.length - 1)];
        }

        public Direction right() {
            return VALUES[index(ordinal() + 1)];
        }

        private int index(int index) {
            return index % VALUES.length;
        }
    }

    public Day22(String input) {
        this.input = input;
    }

    public int infecting1(int bursts) {
        int infections = 0;
        Direction direction = N;
        char[][] grid = grid(bursts);
        int[] pos = {bursts/2, bursts/2};

        for (int i = 0; i < bursts; i++) {
            switch(getGridCell(grid, pos)) {
                case INFECTED:
                    setGridCell(grid, pos, CLEAN);
                    direction = direction.right();
                    break;
                default:
                    infections++;
                    setGridCell(grid, pos, INFECTED);
                    direction = direction.left();
                    break;
            }

            pos = direction.nextPosition(pos);
        }

        return infections;
    }

    public int infecting2(int bursts) {
        int infections = 0;
        int[] pos = {0, 0};
        Direction direction = N;
        Map<String, Character> grid = grid();

        for (int i = 0; i < bursts; i++) {
            switch(getGridMapCell(grid, pos)) {
                case WEAKENED:
                    infections++;
                    setGridMapCell(grid, pos, INFECTED);
                    break;
                case FLAGGED:
                    setGridMapCell(grid, pos, CLEAN);
                    direction = direction.left().left();
                    break;
                case INFECTED:
                    setGridMapCell(grid, pos, FLAGGED);
                    direction = direction.right();
                    break;
                default:
                    setGridMapCell(grid, pos, WEAKENED);
                    direction = direction.left();
                    break;
            }

            pos = direction.nextPosition(pos);
        }

        return infections;
    }

    private char getGridCell(char[][] grid, int[] pos) {
        return grid[pos[0]][pos[1]];
    }

    private void setGridCell(char[][] grid, int[] pos, char c) {
        grid[pos[0]][pos[1]] = c;
    }

    private char getGridMapCell(Map<String, Character> grid, int[] pos) {
        return grid.getOrDefault(pos[0] + "/" + pos[1], CLEAN);
    }

    private void setGridMapCell(Map<String, Character> grid, int[] pos, char c) {
        grid.put(pos[0] + "/" + pos[1], c);
    }

    private char[][] grid(int size) {
        char[][] grid = new char[size][size];
        char[][] map = map();

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                grid[size/2 - map.length/2 + i][size/2 - map[i].length/2 + j] = map[i][j];
            }
        }

        return grid;
    }

    private Map<String, Character> grid() {
        char[][] map = map();
        Map<String, Character> grid = new HashMap<>();

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                int[] pos = {i - map.length/2, j - map[i].length/2};
                setGridMapCell(grid, pos, map[i][j]);
            }
        }

        return grid;
    }

    private char[][] map() {
        return stream(input.split("\n")).map(String::toCharArray).toArray(char[][]::new);
    }
}