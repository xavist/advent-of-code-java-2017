package com.xavi.aoc2017;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

public class Day06 {
    private final String input;

    public Day06(String input) {
        this.input = input;
    }

    public int[] redistribution() {
        int steps = 0;
        Integer previousStep = 0;
        int[] memBanks = memBanks();
        Map<String, Integer> configs = new HashMap<>();

        while ((previousStep = configs.put(config(memBanks), steps)) == null) {
            int max = stream(memBanks).max().getAsInt();
            int index = IntStream.range(0, memBanks.length).filter(i -> memBanks[i] == max).findFirst().getAsInt();

            memBanks[index] = 0;
            for (int i = 0; i < max; i++) {
                index = (index + 1) % memBanks.length;
                memBanks[index]++;
            }

            steps++;
        }

        int[] result = {steps, steps - previousStep};
        return result;
    }

    private String config(int[] memBanks) {
        return stream(memBanks).mapToObj(String::valueOf).collect(joining());
    }

    private int[] memBanks() {
        return stream(input.split(" ")).mapToInt(Integer::parseInt).toArray();
    }
}