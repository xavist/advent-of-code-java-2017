package com.xavi.aoc2017;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

public class Day11 {
    private final String input;

    enum Direction {
        N, NE, SE, S, SW, NW;

        private static final Direction[] VALUES = values();

        public Direction opposite() {
            return VALUES[index(ordinal() + VALUES.length / 2)];
        }

        public Direction left() {
            return VALUES[index(ordinal() + VALUES.length - 1)];
        }

        public Direction right() {
            return VALUES[index(ordinal() + 1)];
        }

        private int index(int index) {
            return index % VALUES.length;
        }
    }

    public Day11(String input) {
        this.input = input;
    }

    public int[] distance() {
        int max = 0;
        int distance = 0;
        int[] normalizedSteps = new int[Direction.values().length];
        List<Direction> steps = steps();

        for (Direction step : steps) {
            distance += normalize(step, normalizedSteps);
            if (distance > max) {
                max = distance;
            }
        }

        int[] result = {distance, max};
        return result;
    }

    private int normalize(Direction step, int[] normalizedSteps) {
        Direction opposite = step.opposite();
        if (normalizedSteps[opposite.ordinal()] > 0) {
            normalizedSteps[opposite.ordinal()]--;
            return -1;
        }

        Direction oppositeLeft = opposite.left();
        if (normalizedSteps[oppositeLeft.ordinal()] > 0) {
            normalizedSteps[oppositeLeft.ordinal()]--;
            return normalize(step.right(), normalizedSteps) - 1;
        }

        Direction oppositeRight = opposite.right();
        if (normalizedSteps[oppositeRight.ordinal()] > 0) {
            normalizedSteps[oppositeRight.ordinal()]--;
            return normalize(step.left(), normalizedSteps) - 1;
        }

        normalizedSteps[step.ordinal()]++;
        return 1;
    }

    private List<Direction> steps() {
        return stream(input.split(",")).map(String::toUpperCase).map(Direction::valueOf).collect(toList());
    }
}