package com.xavi.aoc2017;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

public class Day21 {
    private final String input;
    private final Image INITIAL = new Image(".#./..#/###");

    public Day21(String input) {
        this.input = input;
    }

    public long on(int iterations) {
        Image result = INITIAL;
        Map<String, String> rules = rules();

        for (int it = 0; it < iterations; it++) {
            Image[][] split = result.split();

            for (int i = 0; i < split.length; i++) {
                for (int j = 0; j < split.length; j++) {
                    split[i][j] = new Image(rules.get(split[i][j].toString()));
                }
            }

            result = result.compose(split);
        }

        return result.toString().chars().filter(c -> c == '#').count();
    }

    private Map<String, String> rules() {
        return stream(input.split("\n")).map(s -> s.split(" => ")).flatMap(this::variations).collect(toMap(a -> a[0], a -> a[1], (a, b) -> a));
    }

    private Stream<String[]> variations(String[] rule) {
        String enhacement = rule[1];
        Image image = new Image(rule[0]);
        Image flipped = image.flip();

        Stream.Builder<String[]> variations = Stream.builder();
        variations.add(rule(image, enhacement)).add(rule(flipped, enhacement));

        for (int i = 0; i < 3; i++) {
            image = image.rotate();
            flipped = flipped.rotate();
            variations.add(rule(image, enhacement)).add(rule(flipped, enhacement));
        }

        return variations.build();
    }

    String[] rule(Image pattern, String result) {
        return new String[] {pattern.toString(), result};
    }

    private class Image {
        int size;
        char[][] pixels;

        public Image(String s) {
            this(stream(s.split("/")).map(String::toCharArray).toArray(char[][]::new));
        }

        private Image(char[][] pixels) {
            this.size = pixels.length;
            this.pixels = pixels;
        }

        Image[][] split() {
            return size % 2 == 0 ? split(2) : split(3);
        }

        Image[][] split(int subSize) {
            int splitSize = size / subSize;
            Image[][] split = new Image[splitSize][splitSize];

            for (int i = 0; i < splitSize; i++) {
                for (int j = 0; j < splitSize; j++) {
                    char[][] sub = new char[subSize][subSize];

                    for (int k = 0; k < subSize; k++) {
                        for (int l = 0; l < subSize; l++) {
                            sub[k][l] = pixels[i*subSize + k][j*subSize + l];
                        }
                    }

                    split[i][j] = new Image(sub);
                }
            }

            return split;
        }

        Image compose(Image[][] images) {
            int composedSize = images.length * images[0][0].size;
            char[][] composed = new char[composedSize][composedSize];

            for (int i = 0; i < images.length; i++) {
                for (int j = 0; j < images.length; j++) {
                    for (int k = 0; k < images[i][j].size; k++) {
                        for (int l = 0; l < images[i][j].size; l++) {
                            composed[i*images[i][j].size + k][j*images[i][j].size + l] = images[i][j].pixels[k][l];
                        }
                    }
                }
            }

            return new Image(composed);
        }

        Image rotate() {
            char[][] rotated =  new char[size][size];

            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    rotated[i][j] = pixels[j][size-i-1];
                }
            }

            return new Image(rotated);
        }

        Image flip() {
            char[][] flipped =  new char[size][size];

            for (int i = 0; i < size; i++) {
                flipped[i] = Arrays.copyOf(pixels[size-i-1], size);
            }

            return new Image(flipped);
        }

        public String toString() {
            return stream(pixels).map(String::valueOf).collect(joining("/"));
        }
    }
}