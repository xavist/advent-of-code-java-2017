package com.xavi.aoc2017;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.stream;

public class Day23 {
    private enum Status { RUN, TERMINATED };

    private final String input;

    public Day23(String input) {
        this.input = input;
    }

    public long mul() {
        Program program = new Program(instructions());

        while (program.run().status != Status.TERMINATED);

        return program.numMul;
    }

    public long h() {
        int b = 57 * 100 + 100000;
        int c = b + 17000;

        int nonPrimes = 0;
        for (int i = c; i >= b; i -= 17) {
          if (!BigInteger.valueOf(i).isProbablePrime(100)) nonPrimes++;
        }

        return nonPrimes;
    }

    private Instruction[] instructions() {
        return stream(input.split("\n")).map(Instruction::new).toArray(Instruction[]::new);
    }

    private class Program {
        long next = 0;
        int numMul = 0;
        Status status;
        Instruction[] instructions;
        Map<String, Long> registers = new HashMap<>();

        Program(Instruction[] instructions) {
            this.instructions = instructions;
        }

        Program run() {
            Instruction ins = instructions[(int)next];

            next++;
            status = Status.RUN;

            switch (ins.op) {
                case "set":
                    registers.put(ins.p1, getValue(ins.p2));
                    break;
                case "sub":
                    registers.put(ins.p1, getValue(ins.p1) - getValue(ins.p2));
                    break;
                case "mul":
                    numMul++;
                    registers.put(ins.p1, getValue(ins.p1) * getValue(ins.p2));
                    break;
                case "jnz":
                    if (getValue(ins.p1) != 0) {
                        next += getValue(ins.p2) - 1;
                    }
            }

            if (next < 0 || next >= instructions.length) {
                status = Status.TERMINATED;
            }

            return this;
        }

        long getValue(String p) {
            return p.charAt(0) > '9'? registers.getOrDefault(p, 0L) : Integer.parseInt(p);
        }
    }

    private class Instruction {
        final String op;
        final String p1;
        final String p2;

        Instruction(String instruction) {
            String[] ins = instruction.split(" ");
            this.op = ins[0];
            this.p1 = ins[1];
            this.p2 = ins.length == 3 ? ins[2] : "";
        }
    }
}