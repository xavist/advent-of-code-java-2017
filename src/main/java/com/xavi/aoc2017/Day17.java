package com.xavi.aoc2017;

import java.util.ArrayList;
import java.util.List;

public class Day17 {
    private static final int SIZE1 = 2018;
    private static final int SIZE2 = 50_000_000;

    private final int input;

    public Day17(int input) {
        this.input = input;
    }

    public int value1() {
        int pos = 0;
        List<Integer> spinlock = new ArrayList<>(SIZE1);
        spinlock.add(0);

        for (int i = 1; i < SIZE1; i++) {
            pos = index(pos + input, spinlock.size()) + 1;
            spinlock.add(pos, i);
        }

        return spinlock.get(index(pos + 1, spinlock.size()));
    }

    public int value2() {
        int pos = 0;
        int afterZero = -1;

        for (int i = 1; i < SIZE2; i++) {
            pos = index(pos + input, i) + 1;

            if (pos == 1) {
                afterZero = i;
            }
        }

        return afterZero;
    }

    private int index(int pos, int size) {
        return pos % size;
    }
}