package com.xavi.aoc2017;

import static com.xavi.aoc2017.Day03.Direction.DOWN;

public class Day03 {
    private final int input;

    enum Direction {
        RIGHT(1, 0),
        UP(0, 1),
        LEFT(-1, 0),
        DOWN(0, -1);

        private final int x;
        private final int y;

        private Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Direction nextDirection() {
            final Direction[] values = values();
            return values[(this.ordinal() + 1) % values.length];
        }

        public int[] nextPosition(int[] currentPosition) {
            int[] nextPosition = {currentPosition[0] + x, currentPosition[1] + y};
            return nextPosition;
        }
    }

    public Day03(int input) {
        this.input = input;
    }

    public int distance() {
        if (input == 1) return 0;

        double sqrt = Math.sqrt(input);
        double floored = Math.floor(sqrt);
        // It seems this works for even squares too. Otherwise could add some further conditions to get the proper ring.
        int ring = (int) (sqrt == floored ? sqrt : floored + (floored%2) + 1); // Ring in the spiral where our input cell is
        int topCell = ring * ring;
        int relativePostion = topCell - input;
        int maxDistance = ring - 1; // Distance for the corner cells in the ring
        int relativeDistanceToNext = relativePostion % maxDistance; // Distance from our input cell to next corner cell
        int relativeDistance = Math.min(relativeDistanceToNext, maxDistance - relativeDistanceToNext); // Distance from our input cell to any corner cell

        return maxDistance - relativeDistance;
    }

    public int distance2() {
        if (input == 1) return 0;

        double sqrt = Math.sqrt(input);
        double floored = Math.floor(sqrt);
        boolean isTopCell = sqrt == floored && (sqrt % 2) == 1;
        int ring = (int) (isTopCell ? sqrt : floored + (floored%2) + 1); // Ring in the spiral where our input cell is
        int topCell = ring * ring;
        int relativePostion = topCell - input;
        int maxDistance = ring - 1; // Distance for the corner cells in the ring
        int relativeDistanceToNext = relativePostion % maxDistance; // Distance from our input cell to next corner cell
        int relativeDistance = Math.min(relativeDistanceToNext, maxDistance - relativeDistanceToNext); // Distance from our input cell to any corner cell

        return maxDistance - relativeDistance;
    }

    public int stressTest() {
        // + 4 -> We need extra space for low inputs to calculate the value without checking boundaries
        // and could use less memory in general. Could be optimized.
        int max = (int) Math.floor(Math.sqrt(input)) + 4;
        int[][] matrix = new int[max][max];

        int center = max / 2;
        int[] position = {center, center};
        int result = setValue(matrix, position, 1);
        Direction direction = DOWN;

        while (result <= input) {
            if (getValue(matrix, position) == 0) {
                result = setValue(matrix, position, calculateValue(matrix, position));
            }

            if (getValue(matrix, direction.nextDirection().nextPosition(position)) == 0) {
                direction = direction.nextDirection();
            }

            position = direction.nextPosition(position);
        }

        return result;
    }

    private int getValue(int[][] matrix, int[] position) {
        return matrix[position[0]][position[1]];
    }

    private int setValue(int[][] matrix, int[] position, int value) {
        return matrix[position[0]][position[1]] = value;
    }

    private int calculateValue(int[][] matrix, int[] position) {
        int i = position[0];
        int j = position[1];

        return matrix[i+1][j] +
                matrix[i+1][j+1] +
                matrix[i][j+1] +
                matrix[i-1][j+1] +
                matrix[i-1][j] +
                matrix[i-1][j-1] +
                matrix[i][j-1] +
                matrix[i+1][j-1];
    }
}