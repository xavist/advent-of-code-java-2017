package com.xavi.aoc2017;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.stream;
import static java.util.Collections.EMPTY_LIST;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

public class Day07 {
    private final String input;
    private final Map<String, Program> programs;

    public Day07(String input) {
        this.input = input;
        programs = programs().collect(toMap(Program::getName, identity()));
    }

    public String bottom() {
        Set<String> inTower = programs.values().stream().map(Program::getTower).flatMap(Arrays::stream).collect(toSet());

        return programs.keySet().stream().filter(name -> !inTower.contains(name)).findAny().get();
    }

    public int weight() {
        final Program bottom = programs.get(bottom());
        return bottom.getBalancingWeight(EMPTY_LIST);
    }

    private Stream<Program> programs() {
        return stream(input.split("\n")).map(Program::new);
    }

    private class Program {
        private final String name;
        private final int weight;
        private final String[] tower;
        private int totalWeight;

        public Program(String program) {
            String[] rep = program.split(" -> ");
            String[] status = rep[0].split(" ");

            this.name = status[0];
            this.weight = parseInt(status[1].substring(1, status[1].length() - 1));
            this.tower = rep.length > 1 ? rep[1].split(", ") : new String[0];
            this.totalWeight = -1;
        }

        public String getName() {
            return name;
        }

        public int getWeight() {
            if (totalWeight == -1) {
                totalWeight = weight + stream(tower).map(programs::get).mapToInt(Program::getWeight).sum();
            }

            return totalWeight;
        }

        public int getBalancingWeight(List<Program> siblings) {
            if (tower.length > 0) { // Check children balance
                List<Program> children = stream(tower).map(programs::get).collect(Collectors.toList());
                int balancingWeight = children.stream().mapToInt(c -> c.getBalancingWeight(children)).sum();
                if (balancingWeight != 0) return balancingWeight;
            }

            int[] otherWeights = siblings.stream().mapToInt(Program::getWeight).filter(w -> w != getWeight()).toArray();
            if (otherWeights.length > 1) { // Unbalanced
                return weight + otherWeights[0] - getWeight();
            }

            return 0;
        }

        public String[] getTower() {
            return tower;
        }
    }
}