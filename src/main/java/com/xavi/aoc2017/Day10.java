package com.xavi.aoc2017;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Arrays.stream;
import static java.util.stream.IntStream.concat;
import static java.util.stream.IntStream.range;

public class Day10 {
    private final int size;
    private final String input;

    public Day10(String input) {
        this(input, 256);
    }

    public Day10(String input, int size) {
        this.size = size;
        this.input = input;
    }

    public int round() {
        int[] list = hash(1, lengths1());
        return stream(list).limit(2).reduce(1, Math::multiplyExact);
    }

    public String knotHash() {
        int[] list = hash(64, lengths2());
        return IntStream.range(0, 16)
                .map(i -> i*16)
                .mapToObj(i -> stream(list, i, i + 16))
                .map(s -> s.reduce((x, y) -> x ^ y).orElse(0))
                .map(Integer::toHexString)
                .map(s -> s.length() == 1 ? "0" + s : s)
                .collect(Collectors.joining());
    }

    private int[] hash(int numRounds, int[] lengths) {
        int skip = 0;
        int position = 0;
        int[] list = range(0, size).toArray();

        for (int r = 0; r < numRounds; r++) {
            for (int i = 0; i < lengths.length; i++) {
                int length = lengths[i];
                int mid = length / 2;

                for (int j = 0; j < mid; j++) {
                    int p1 = index(position + j);
                    int p2 = index(position + length -1 - j);
                    int tmp = list[p1];

                    list[p1] = list[p2];
                    list[p2] = tmp;
                }

                position = index(position + length + skip);
                skip++;
            }
        }

        return list;
    }

    private int index(int index) {
        return index % size;
    }

    private int[] lengths1() {
        return stream(input.split(",")).mapToInt(Integer::parseInt).toArray();
    }

    private int[] lengths2() {
        return concat(input.chars(), IntStream.of(17, 31, 73, 47, 23)).toArray();
    }
}