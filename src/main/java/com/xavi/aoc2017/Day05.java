package com.xavi.aoc2017;

import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.stream;

public class Day05 {
    private final String input;

    public Day05(String input) {
        this.input = input;
    }

    public int steps1() {
        return steps(MAX_VALUE);
    }

    public int steps2() {
        return steps(3);
    }

    private int steps(int flag) {
        int steps = 0;
        int index = 0;
        int[] instructions = instructions();
        while (index >= 0 && index < instructions.length) {
            int offset = instructions[index];

            if (offset >= flag) {
                instructions[index]--;
            } else {
                instructions[index]++;
            }

            index += offset;
            steps++;
        }
        return steps;
    }

    private int[] instructions() {
        return stream(input.split("\n")).mapToInt(Integer::parseInt).toArray();
    }
}