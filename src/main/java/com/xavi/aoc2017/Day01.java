package com.xavi.aoc2017;

import java.util.stream.IntStream;

public class Day01 {
    private final String input;

    public Day01(String input) {
        this.input = input;
    }

    public int captcha1() {
        return captcha(1);
    }

    public int captcha2() {
        return captcha(input.length()/2);
    }

    private int captcha(int offset) {
        return IntStream.range(0, input.length())
                .filter(i -> offsetEquals(i, offset))
                .map(input::charAt)
                .map(Character::getNumericValue)
                .sum();
    }

    private boolean offsetEquals(int position, int offset) {
        return input.charAt(position) == input.charAt((position + offset) % input.length());
    }
}