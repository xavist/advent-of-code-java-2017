package com.xavi.aoc2017;


import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static java.util.Collections.EMPTY_LIST;
import static java.util.stream.Collectors.toList;

public class Day24 {
    private final String input;

    public Day24(String input) {
        this.input = input;
    }

    public long strongest() {
        List<Component> strongest = bridge(0, components(), Comparator.comparingInt(this::strenght));
        return strenght(strongest);
    }

    public long longest() {
        List<Component> longest = bridge(0, components(), Comparator.comparingInt(this::length).thenComparing(this::strenght));
        return strenght(longest);
    }

    private int strenght(List<Component> bridge) {
        return bridge.stream().mapToInt(Component::strenght).sum();
    }

    private int length(List<Component> bridge) {
        return bridge.size();
    }

    private List<Component> bridge(int port, List<Component> components, Comparator<List<Component>> comparator) {
        return components.stream()
                .filter(c1 -> c1.bind(port))
                .map(c1 -> bridge(c1, components, comparator))
                .max(comparator)
                .orElse(EMPTY_LIST);
    }

    private List<Component> bridge(Component c1, List<Component> components, Comparator<List<Component>> comparator) {
        List<Component> rest = components.stream().filter(c2 -> c1 != c2).collect(toList());
        List<Component> bridge = bridge(c1.p2, rest, comparator);

        return Stream.concat(Stream.of(c1), bridge.stream()).collect(toList());
    }

    private List<Component> components() {
        return stream(input.split("\n")).map(Component::new).collect(toList());
    }

    private class Component {
        int p1;
        int p2;

        Component(String component) {
            String[] c = component.split("/");
            this.p1 = Integer.parseInt(c[0]);
            this.p2 = Integer.parseInt(c[1]);
        }

        boolean bind(int p) {
            if (p1 == p) return true;
            if (p2 == p) {
                p2 = p1;
                p1 = p;
                return true;
            }

            return false;
        }

        int strenght() {
            return p1 + p2;
        }
    }
}