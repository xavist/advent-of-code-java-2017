package com.xavi.aoc2017;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.stream;

public class Day08 {
    private final String input;
    private final Map<String, Integer> registers;

    public Day08(String input) {
        this.input = input;
        registers = new HashMap<>();
    }

    public int[] largest() {
        final int max = instructions().mapToInt(Instruction::execute).max().orElse(0);
        final int end = registers.values().stream().max(Integer::compare).orElse(0);

        int[] result = {end, max};
        return result;
    }

    private Stream<Instruction> instructions() {
        return stream(input.split("\n")).map(Instruction::new);
    }

    private class Instruction {
        private final String mr;
        private final String mo;
        private final int ma;
        private final String cr;
        private final String co;
        private final int ca;

        public Instruction(String instruction) {
            String[] ins = instruction.split(" ");
            this.mr = ins[0];
            this.mo = ins[1];
            this.ma = parseInt(ins[2]);
            this.cr = ins[4];
            this.co = ins[5];
            this.ca = parseInt(ins[6]);

            registers.putIfAbsent(mr, 0);
            registers.putIfAbsent(cr, 0);
        }

        public int execute() {
            return condition() ? registers.compute(mr, this::operation) : 0;
        }

        private boolean condition() {
            int cv = registers.get(cr);

            switch (co) {
                case ">" : return cv > ca;
                case "<" : return cv < ca;
                case ">=": return cv >= ca;
                case "<=": return cv <= ca;
                case "==": return cv == ca;
                case "!=": return cv != ca;
            }

            return false;
        }

        private int operation(String mr, int mv) {
            switch (mo) {
                case "inc": return mv + ma;
                case "dec": return mv - ma;
            }

            return mv;
        }
    }
}