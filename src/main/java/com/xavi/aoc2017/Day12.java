package com.xavi.aoc2017;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toMap;

public class Day12 {
    private final String input;
    private final Map<String, String[]> piping;

    public Day12(String input) {
        this.input = input;
        this.piping = piping();
    }

    public int programs() {
        return group("0", new HashSet<>()).size();
    }

    public int groups() {
        List<Set<String>> groups = new ArrayList<>();

        for (String program : piping.keySet()) {
            if (groups.stream().noneMatch(group -> group.contains(program))) {
                groups.add(group(program, new HashSet<>()));
            }
        }

        return groups.size();
    }

    private Set<String> group(String program, Set<String> programs) {
        if (programs.add(program)) {
            stream(piping.get(program)).forEach(p -> group(p, programs));
        }

        return programs;
    }

    private Map<String, String[]> piping() {
        return stream(input.split("\n")).map(s -> s.split(" <-> ")).collect(toMap(p -> p[0], p -> p[1].split(", ")));
    }
}