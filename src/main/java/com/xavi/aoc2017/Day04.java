package com.xavi.aoc2017;

import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

public class Day04 {
    private final String input;

    public Day04(String input) {
        this.input = input;
    }

    public long numValid1() {
       return rows()
               .map(this::cols)
               .filter(cols -> cols.length == stream(cols).distinct().count())
               .count();
    }

    public long numValid2() {
       return rows()
               .map(this::cols)
               .filter(cols -> cols.length == stream(cols).map(this::sort).distinct().count())
               .count();
    }

    private Stream<String> rows() {
        return stream(input.split("\n"));
    }

    private String[] cols(String row) {
        return row.split(" ");
    }

    private String sort(String word) {
        return stream(word.split("")).sorted().collect(joining());
    }
}