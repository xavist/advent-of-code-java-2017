package com.xavi.aoc2017;

public class Day09 {
    private final String input;

    public Day09(String input) {
        this.input = input;
    }

    public int[] score() {
        int score = 0;
        int nesting = 0;
        int counter = 0;
        boolean garbage = false;

        for (int i = 0; i < input.length(); i++) {
            switch (input.charAt(i)) {
                case '{': if (garbage) counter++; else nesting++;
                    break;
                case '}': if (garbage) counter++; else score += nesting--;
                    break;
                case '<': if (garbage) counter++; else garbage = true;
                    break;
                case '>': garbage = false;
                    break;
                case '!': i++;
                    break;
                default: if (garbage) counter++;
            }
        }

        int[] result = {score, counter};
        return result;
    }
}