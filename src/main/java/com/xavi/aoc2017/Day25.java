package com.xavi.aoc2017;

import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public class Day25 {
    private String state;
    private final int steps;
    private final String input;

    public Day25(String state, int steps, String input) {
        this.state = state;
        this.steps = steps;
        this.input = input;
    }

    public long checksum() {
        int pos = 0;
        Map<Integer, Integer> tape = new HashMap<>();
        Map<String, State> states = states();

        for (int i = 0; i < steps; i++) {
            final Integer value = tape.getOrDefault(pos, 0);
            Instruction ins = states.get(state).ins[value];

            if (!ins.write.equals(value)) {
                if (ins.write == 1) {
                    tape.put(pos, 1);
                } else {
                    tape.remove(pos);
                }
            }

            if ("left".equals(ins.move)) {
                pos--;
            } else {
                pos++;
            }

            state = ins.state;
        }

        return tape.size();
    }

    private Map<String, State> states() {
        return stream(input.split("\n\n")).map(State::new).collect(toMap(state -> state.id, identity()));
    }

    private class State {
        final String id;
        final Instruction[] ins;

        State(String state) {
            int index = state.indexOf(":");
            this.id = state.substring(index - 1, index);

            int ins1 = state.indexOf(":", index + 1) + 2;
            int ins2 = state.indexOf(":", ins1 + 1) + 2;
            ins = new Instruction[2];
            ins[0] = new Instruction(state.substring(ins1, ins2));
            ins[1] = new Instruction(state.substring(ins2));

        }
    }

    private class Instruction {
        final Integer write;
        final String move;
        final String state;

        Instruction(String instruction) {
            String[] ins = instruction.split("\n");
            this.write = Integer.valueOf(ins[0].substring(ins[0].lastIndexOf(" ") + 1, ins[0].indexOf(".")));
            this.move = ins[1].substring(ins[1].lastIndexOf(" ") + 1, ins[1].indexOf("."));
            this.state = ins[2].substring(ins[2].lastIndexOf(" ") + 1, ins[2].indexOf("."));
        }
    }
}