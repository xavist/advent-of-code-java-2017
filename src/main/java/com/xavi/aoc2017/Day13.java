package com.xavi.aoc2017;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

public class Day13 {
    private final String input;

    private List<Scanner> firewall;
    private int numLayers;

    public Day13(String input) {
        this.input = input;
    }

    public int severity() {
        setupFirewall(); // Start from scratch
        return sendPacket();
    }

    public long delay() {
        long delay = 0;
        setupFirewall(); // Start from scratch

        while (!sendPacketUndetected()) {
            delay++;
            delayPacket();
        }

        return delay;
    }

    private int sendPacket() {
        IntStream.rangeClosed(0, numLayers).forEach(packetLayer -> firewall.forEach(scanner -> scanner.move(packetLayer)));
        return firewall.stream().mapToInt(Scanner::severity).sum();
    }

    private boolean sendPacketUndetected() {
        for (int i = 0; i <= numLayers; i++) {
            for (Scanner scanner : firewall) {
                if (scanner.move(i).caught) {
                    return false;
                }
            }
        }

        return true;
    }

    private void delayPacket() {
        firewall.forEach(Scanner::reset);
        firewall.forEach(scanner -> scanner.move(-1));
        firewall.forEach(Scanner::snapshot);
    }

    private void setupFirewall() {
        this.firewall = stream(input.split("\n")).map(Scanner::new).collect(toList());
        this.numLayers = firewall.get(firewall.size()-1).layer;
    }

    private final class Scanner {
        private static final int UP = -1;
        private static final int DOWN = 1;

        int layer;
        int range;
        int position;
        int direction;
        boolean caught;

        int snapshotPosition;
        int snapshotDirection;

        Scanner(String scanner) {
            String[] conf = scanner.split(": ");
            this.layer = Integer.parseInt(conf[0]);
            this.range = Integer.parseInt(conf[1]);
            this.position = 1;
            this.direction = DOWN;
            snapshot();
        }

        Scanner move(int packetLayer) {
            if (!caught) {
                caught = packetLayer == layer && position == 1;
            }

            if (position == 1) {
                direction = DOWN;
            } else if (position == range) {
                direction = UP;
            }

            position += direction;
            return this;
        }

        int severity() {
            return caught ? layer * range : 0;
        }

        void snapshot() {
            snapshotPosition = position;
            snapshotDirection = direction;
        }

        void reset() {
            position = snapshotPosition;
            direction = snapshotDirection;
            caught = false;
        }
    }
}