package com.xavi.aoc2017;

import java.util.IntSummaryStatistics;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Arrays.stream;

public class Day02 {
    private final String input;

    public Day02(String input) {
        this.input = input;
    }

    public int checksum1() {
       return rows().mapToInt(this::rowChecksum1).sum();
    }

    public int checksum2() {
       return rows().mapToInt(this::rowChecksum2).sum();
    }

    private int rowChecksum1(String row) {
        final IntSummaryStatistics stats = cols(row).summaryStatistics();
        return stats.getMax() - stats.getMin();
    }

    private int rowChecksum2(String row) {
        int[] cols = cols(row).sorted().toArray();

        for (int i = 0; i < cols.length; i++) {
            for (int j = i+1; j < cols.length; j++) {
                if (cols[j] % cols[i] == 0) {
                    return cols[j] / cols[i];
                }
            }
        }

        return 0;
    }

    private Stream<String> rows() {
        return stream(input.split("\n"));
    }

    private IntStream cols(String row) {
        return stream(row.split("\\s")).mapToInt(Integer::parseInt);
    }
}