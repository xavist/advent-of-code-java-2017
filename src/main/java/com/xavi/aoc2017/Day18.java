package com.xavi.aoc2017;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.stream;

public class Day18 {
    private enum Status { RUN, RECEIVE, TERMINATED };

    private final String input;

    public Day18(String input) {
        this.input = input;
    }

    public long sound() {
        Program program = new Program(0, instructions());
        program.init(program);

        while (program.run().status != Status.TERMINATED);

        return program.messages.get(program.messages.size() - 1);
    }

    public int messages() {
        final Instruction[] instructions = instructions();
        Program program0 = new Program(0, instructions);
        Program program1 = new Program(1, instructions);
        program0.init(program1);
        program1.init(program0);

        while (program1.status != Status.TERMINATED && (program0.status != Status.RECEIVE || program1.status != Status.RECEIVE)) {
            program0.run();
            program1.run();
        }

        return program1.numSentMessages;
    }

    private Instruction[] instructions() {
        return stream(input.split("\n")).map(Instruction::new).toArray(Instruction[]::new);
    }

    private class Program {
        long next;
        int numSentMessages;
        Program program;
        Status status;
        Instruction[] instructions;
        List<Long> messages = new ArrayList<>();
        Map<String, Long> registers = new HashMap<>();

        Program(long id, Instruction[] instructions) {
            registers.put("p", id);
            this.instructions = instructions;
        }

        void init(Program program) {
            this.program = program;
        }

        void message(long message) {
            messages.add(message);
        }

        Program run() {
            Instruction ins = instructions[(int)next];

            next++;
            status = Status.RUN;

            switch (ins.op) {
                case "snd":
                    long value = getValue(ins.p1);
                    program.message(value);
                    numSentMessages++;
                    break;
                case "set":
                    registers.put(ins.p1, getValue(ins.p2));
                    break;
                case "add":
                    registers.put(ins.p1, getValue(ins.p1) + getValue(ins.p2));
                    break;
                case "mul":
                    registers.put(ins.p1, getValue(ins.p1) * getValue(ins.p2));
                    break;
                case "mod":
                    registers.put(ins.p1, getValue(ins.p1) % getValue(ins.p2));
                    break;
                case "rcv":
                    if (program == this) {
                        if (getValue(ins.p1) != 0) status = Status.TERMINATED;
                    } else {
                        if (messages.isEmpty()) {
                            next--;
                            status = Status.RECEIVE;
                        } else {
                           registers.put(ins.p1, messages.remove(0));
                        }
                    }
                    break;
                case "jgz":
                    if (getValue(ins.p1) > 0) {
                        next += getValue(ins.p2) - 1;
                        if (next < 0 || next >= instructions.length) {
                            status = Status.TERMINATED;
                        }
                    }
            }

            return this;
        }

        long getValue(String p) {
            return p.charAt(0) > '9'? registers.getOrDefault(p, 0L) : Integer.parseInt(p);
        }
    }

    private class Instruction {
        final String op;
        final String p1;
        final String p2;

        Instruction(String instruction) {
            String[] ins = instruction.split(" ");
            this.op = ins[0];
            this.p1 = ins[1];
            this.p2 = ins.length == 3 ? ins[2] : "";
        }
    }
}