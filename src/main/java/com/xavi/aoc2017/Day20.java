package com.xavi.aoc2017;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.lang.Boolean.TRUE;
import static java.lang.Integer.compare;
import static java.lang.Integer.signum;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class Day20 {
    private final String input;

    public Day20(String input) {
        this.input = input;
    }

    public int closest() {
        boolean systemStable = false;
        List<Particle> particles = particles();

        while (!systemStable) {
            particles.forEach(Particle::update);
            systemStable = particles.stream().allMatch(Particle::isStable);
        }

        Particle min = particles.stream().min(this::compareCloserToCenterLongTermWhenSystemStable).get();
        return particles.indexOf(min);
    }

    private int compareCloserToCenterLongTermWhenSystemStable(Particle a, Particle b) {
        int a1 = manhattan(a.a);
        int a2 = manhattan(b.a);
        if (a1 != a2) return compare(a1, a2);

        int v1 = manhattan(a.v);
        int v2 = manhattan(b.v);
        if (v1 != v2) return compare(v1, v2);

        int p1 = manhattan(a.p);
        int p2 = manhattan(b.p);
        return compare(p1, p2);
    }

    // Starts looking for collision free pairs when particles become stable (best of both worlds from other implementations below).
    // Still a bit slower than the approach below for my input, but might be the most correct for all cases.
    public int left() {
        boolean collisionFree = false;
        List<Particle> particles = particles();
        Set<Particle> stable = new HashSet<>();
        List<ParticlePair> pairs = new ArrayList<>();

        while (!collisionFree) {
            List<Particle> collided = particles.stream()
                .collect(groupingBy(Particle::position))
                .values().stream()
                .filter(list -> list.size() > 1)
                .flatMap(List::stream)
                .collect(toList());

            particles.removeAll(collided);
            stable.removeAll(collided);

            List<Particle> newStable = particles.stream().filter(Particle::isStable).filter(stable::add).collect(toList());
            pairs.addAll(pairs(newStable, stable));

            pairs = pairs.stream()
                    .filter(pair -> !collided.contains(pair.a) && !collided.contains(pair.b))
                    .filter(pair -> !pair.collisionFree())
                    .collect(toList());

            collisionFree = stable.size() == particles.size() && pairs.isEmpty();

            particles.forEach(Particle::update);
        }

        return particles.size();
    }

    // Only starts looking for collision free pairs after the whole system has become stable
    public int left2() {
        boolean systemStable = false;
        boolean collisionFree = false;
        List<Particle> particles = particles();
        List<ParticlePair> pairs = new ArrayList<>();

        while (!collisionFree) {
            List<Particle> collided = particles.stream()
                .collect(groupingBy(Particle::position))
                .values().stream()
                .filter(list -> list.size() > 1)
                .flatMap(List::stream)
                .collect(toList());

            particles.removeAll(collided);

            if (!systemStable) {
                systemStable = particles.stream().allMatch(Particle::isStable);

                if (systemStable) {
                    pairs = pairs(particles);
                }
            }

            if (systemStable) {
                pairs = pairs.stream()
                        .filter(pair -> !collided.contains(pair.a) && !collided.contains(pair.b))
                        .filter(pair -> !pair.collisionFree())
                        .collect(toList());
                collisionFree = pairs.isEmpty();
            }

            particles.forEach(Particle::update);
        }

        return particles.size();
    }

    // Starts working with pairs from the beginning.
    // Much slower due to dealing with a big number of pairs and
    // no collision free particles getting initially closer for a very long time after the system becomes stable
    // (which in some very specific cases could make the approach above slower).
    public int left3() {
        boolean collisionFree = false;
        List<Particle> particles = particles();
        List<ParticlePair> pairs = pairs(particles);

        while (!collisionFree) {
            List<Particle> collided = particles.stream()
                .collect(groupingBy(Particle::position))
                .values().stream()
                .filter(list -> list.size() > 1)
                .flatMap(List::stream)
                .collect(toList());

            particles.removeAll(collided);

            pairs = pairs.stream()
                        .filter(pair -> !collided.contains(pair.a) && !collided.contains(pair.b))
                        .filter(pair -> !pair.collisionFree())
                        .collect(toList());
            collisionFree = pairs.isEmpty();

            particles.forEach(Particle::update);
        }

        return particles.size();
    }

    private Set<ParticlePair> pairs(List<Particle> newStable, Set<Particle> stable) {
        return newStable.stream()
                .flatMap(p1 ->
                        stable.stream()
                                .filter(p2 -> !newStable.subList(0, newStable.indexOf(p1) + 1).contains(p2))
                                .map(p2 -> new ParticlePair(p1, p2))
                )
                .collect(toSet());
    }

    private List<ParticlePair> pairs(List<Particle> particles) {
        return particles.stream()
                .flatMap(p1 ->
                        particles.subList(particles.indexOf(p1) + 1, particles.size()).stream().map(p2 -> new ParticlePair(p1, p2))
                )
                .collect(toList());
    }

    private int distance(int[] a, int[] b) {
        int[] p = IntStream.range(0, a.length).map(i -> a[i] - b[i]).toArray();
        return manhattan(p);
    }

    private int manhattan(int[] vector) {
        return stream(vector).map(Math::abs).sum();
    }

    private List<Particle> particles() {
        return stream(input.split("\n")).map(Particle::new).collect((toList()));
    }

    private class ParticlePair {
        final Particle a;
        final Particle b;
        boolean collisionFree;

        public ParticlePair(Particle a, Particle b) {
            this.a = a;
            this.b = b;
            this.collisionFree = false;
        }

        boolean collisionFree() {
            return a.isStable() && b.isStable() && distance(a.p, b.p) >= distance(a.pp, b.pp);
        }

        Stream<Particle> stream() {
            return Stream.of(a, b);
        }
    }

    private class Particle {
        int[] pp; //Previous position
        int[] p;
        int[] v;
        int[] a;

        public Particle(String particle) {
            String[] components = particle.split(", ");
            this.pp = component(components[0]);
            this.p = component(components[0]);
            this.v = component(components[1]);
            this.a = component(components[2]);
        }

        boolean isStable() {
            return hasFixedTrajectory() && isMovingAwayFromCenter();
        }

        boolean isMovingAwayFromCenter() {
            return manhattan(p) > manhattan(pp);
        }

        boolean hasFixedTrajectory() {
            return IntStream.range(0, a.length)
                    .mapToObj(i -> signum(v[i]) == signum(a[i]) || a[i] == 0)
                    .allMatch(TRUE::equals);
        }

        Particle update() {
            for (int i = 0; i < a.length; i++) {
                pp[i] = p[i];
                v[i] += a[i];
                p[i] += v[i];
            }

            return this;
        }

        String position() {
            return stream(p).mapToObj(String::valueOf).collect(joining("-"));
        }

        private int[] component(String component) {
            final String vector = component.substring(3, component.length() -1);
            return stream(vector.split(",")).mapToInt(Integer::parseInt).toArray();
        }
    }
}