package com.xavi.aoc2017;

import java.math.BigInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Stream.concat;
import static java.util.stream.Stream.generate;

public class Day14 {
    private static final int SIZE = 128;
    private static final String EMPTY = generate(() -> "0").limit(SIZE + 2).collect(joining());

    private final String input;

    public Day14(String input) {
        this.input = input;
    }

    public int[] info() {
        int used = 0;
        int group = 2;
        int[][] grid = paddedGrid();

        for (int i = 1; i <= SIZE; i++) {
            for (int j = 1; j <= SIZE; j++) {
                final int groupSize = group(group, i, j, grid);
                used += groupSize;

                if (groupSize > 0) {
                    group++;
                }
            }
        }


        int[] result = {used, group - 2}; //-1 for starting at 2 and -1 for last unused increment
        return result;
    }

    private int group(int group, int i, int j, int[][] grid) {
        if (grid[i][j] == 1) {
            grid[i][j] = group;
            return 1 +
                    group(group, i + 1, j, grid) +
                    group(group, i - 1, j, grid) +
                    group(group, i, j + 1, grid) +
                    group(group, i, j - 1, grid);
        }

        return 0;
    }

    private String toPaddedBinary(String knotHash) {
        final String binary = new BigInteger(knotHash, 16).toString(2);
        final String padding = generate(() -> "0").limit(SIZE - binary.length()).collect(joining());
        return "0" + padding + binary + "0";
    }

    private int[][] paddedGrid() {
        return paddedHashes()
                .map(String::chars)
                .map(s -> s.map(Character::getNumericValue).toArray())
                .toArray(int[][]::new);
    }

    private Stream<String> paddedHashes() {
        return concat(concat(Stream.of(EMPTY), knotHashes().map(this::toPaddedBinary)), Stream.of(EMPTY));
    }

    private Stream<String> knotHashes() {
        return IntStream.range(0, SIZE).mapToObj(r -> input + "-" + r).map(Day10::new).map(Day10::knotHash);
    }
}