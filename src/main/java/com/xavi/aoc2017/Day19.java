package com.xavi.aoc2017;

import java.util.stream.IntStream;

import static com.xavi.aoc2017.Day19.Direction.S;
import static java.util.Arrays.stream;

public class Day19 {
    private final String input;
    private final char[][] tubes;

    enum Direction {
        N(-1, 0),
        E(0, 1),
        S(1, 0),
        W(0, -1);

        private static final Direction[] VALUES = values();

        private final int x;
        private final int y;

        private Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int[] nextPosition(int[] currentPosition) {
            int[] nextPosition = {currentPosition[0] + x, currentPosition[1] + y};
            return nextPosition;
        }

        public Direction left() {
            return VALUES[index(ordinal() + VALUES.length - 1)];
        }

        public Direction right() {
            return VALUES[index(ordinal() + 1)];
        }

        private int index(int index) {
            return index % VALUES.length;
        }
    }

    public Day19(String input) {
        this.input = input;
        this.tubes = tubes();
    }

    public Object[] path() {
        int steps = 0;
        StringBuilder letters = new StringBuilder();
        char cell = '|';
        Direction direction = S;
        int[] pos = {0, IntStream.range(0, tubes[0].length).filter(j -> tubes[0][j] == '|').findFirst().getAsInt()};

        while (cell != ' ') {
            if (cell != '|' && cell != '-' && cell != '+') {
                letters.append(cell);
            }

            Direction nextDirection = direction;
            int[] nextPosition = nextDirection.nextPosition(pos);
            char nextCell = getCell(nextPosition);

            if (nextCell == ' ') {
                nextDirection = direction.left();
                nextPosition = nextDirection.nextPosition(pos);
                nextCell = getCell(nextPosition);

                if (nextCell == ' ') {
                    nextDirection = direction.right();
                    nextPosition = nextDirection.nextPosition(pos);
                    nextCell = getCell(nextPosition);
                }
            }

            direction = nextDirection;
            pos = nextPosition;
            cell = nextCell;
            steps++;
        }

        Object[] result = {letters.toString(), steps};
        return result;
    }

    private char getCell(int[] pos) {
        final int i = pos[0];
        final int j = pos[1];

        if (i < 0 || i >= tubes.length || j < 0 || j >= tubes[i].length) {
            return ' ';
        }

        return tubes[i][j];
    }

    private char[][] tubes() {
        return stream(input.split("\n")).map(String::toCharArray).toArray(char[][]::new);
    }
}