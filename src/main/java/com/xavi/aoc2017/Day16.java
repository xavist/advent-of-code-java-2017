package com.xavi.aoc2017;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.joining;

public class Day16 {
    private final int MAX_SIZE = 1000;
    private static final int ROUNDS = 1000_000_000;

    private final Step[] steps;
    private final StringBuilder programs;

    public Day16(int size, String input) {
        this.steps = stream(input.split(",")).map(Step::new).toArray(Step[]::new);
        this.programs = IntStream.range(0, size)
                .map(i -> 'a' + i)
                .mapToObj(i -> Character.toString((char)i))
                .collect(collectingAndThen(joining(), StringBuilder::new));
    }

    public String[] dance() {
        int round;
        int index = -1;
        String move = programs.toString();
        List<String> moves = new ArrayList<>();
        moves.add(move);

        for (round = 1; round <= ROUNDS; round++) {
            move = move();
            if (round > MAX_SIZE) continue;

            index = moves.indexOf(move);
            if (index != -1) break;
            moves.add(move);
        }

        if (round < ROUNDS) {
            int cycleLength = round - index;
            move = moves.get(ROUNDS % cycleLength);
        }

        String[] result = {moves.get(1), move};
        return result;
    }

    private String move() {
        stream(steps).forEach(step -> {
            switch (step.type) {
                case 's':
                    spin(step.i1);
                    break;
                case 'x':
                    exchange(step.i1, step.i2);
                    break;
                case 'p':
                    partner(step.p1, step.p2);
                    break;
            }
        });

        return programs.toString();
    }

    private void spin(int numPrograms) {
        final int pos = programs.length() - numPrograms;
        final String substring = programs.substring(pos);
        programs.delete(pos, programs.length())
                .insert(0, substring);
    }

    private void exchange(int pos1, int pos2) {
        char tmp = programs.charAt(pos1);
        programs.setCharAt(pos1, programs.charAt(pos2));
        programs.setCharAt(pos2, tmp);
    }

    private void partner(String c1, String c2) {
        exchange(programs.indexOf(c1), programs.indexOf(c2));
    }

    private class Step {
        final char type;
        final String p1;
        final String p2;
        final int i1;
        final int i2;

        public Step(String step) {
            this.type = step.charAt(0);
            int separator = step.indexOf("/");
            if (separator == -1) {
                separator = step.length();
            }

            this.p1 = step.substring(1, separator);
            this.p2 = type == 's' ? "" : step.substring(separator + 1, step.length());
            this.i1 = type == 'p' ? 0 : Integer.parseInt(p1);
            this.i2 = type != 'x' ? 0 : Integer.parseInt(p2);
        }
    }
}