package com.xavi.aoc2017;

import java.util.function.Predicate;

public class Day15 {
    private final static int FACTOR_A = 16807;
    private final static int FACTOR_B = 48271;

    private final int inputA;
    private final int inputB;

    public Day15(int inputA, int inputB) {
        this.inputA = inputA;
        this.inputB = inputB;
    }

    public int matches1() {
        final Generator A = new Generator(inputA, FACTOR_A);
        final Generator B = new Generator(inputB, FACTOR_B);

        return matches(A, B, 40_000_000);
    }

    public int matches2() {
        final Generator A = new Generator(inputA, FACTOR_A, (l) -> l % 4 == 0);
        final Generator B = new Generator(inputB, FACTOR_B, (l) -> l % 8 == 0);

        return matches(A, B, 5_000_000);
    }

    private int matches(Generator A, Generator B, int iterations) {
        int matches = 0;

        for (int i = 0; i < iterations; i++) {
            if ((short)A.next() == (short)B.next()) { // Cast to short is equivalent to mask with & 0xFFFF
                matches++;
            }
        }

        return matches;
    }

    private class Generator {
        long value; // Declared long to prevent overflows
        final int factor;
        final Predicate<Long> filter;

        public Generator(int seed, int factor) {
            this(seed, factor, (l) -> true);
        }

        public Generator(int seed, int factor, Predicate<Long> filter) {
            this.value = seed;
            this.factor = factor;
            this.filter = filter;
        }

        public int next() {
            do {
                value = (value * factor) % Integer.MAX_VALUE;
            } while (!filter.test(value));

            return (int)value;
        }
    }
}